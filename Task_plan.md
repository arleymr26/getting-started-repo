# TE market simulation
The objective of this project is to deploy a small TE market simulation with at least one residential customer and one coordinator. 

## Tasks
1. Configure a RPi device to join into the swarm cluster of the lab.
    1.1 It was needed to install Raspbian and Rasperry Imager is the prefered method
    1.2 In order to join the RPi as a worker node, it was necessary to go to the Lab in the IRH downstairs
2. Connect the Gitlab project repository with the swarm cluster.
    2.1 I can access to the cluster via: https://naren.te.lirei.ca
    2.2 I created a getting-started repo with a hello world python file.
        2.2.1 Installed the GitLab Workflow extension (Extensions menu)
        2.2.2 Verify git version (or install Git) via Terminal. And login (GitHub credentials).
        2.2.3 Started to try Git commands and edit the Repo
3. Create an overlay network using the Docker SDK for python.
    3.1 Follow the tutorial https://docker-py.readthedocs.io/en/stable/
    3.2 Follow until you can undestand networks
4. Deploy a message broker container in the overlay network.
5. Deploy the market agents in the swarm cluster.
6. Store market clearing results in a database.
